use itertools::Itertools;
use rand::distr::{Distribution, Uniform};
use std::{iter, string::String};

fn main() {
    let pin_len = 8;

    let mut rng = rand::rng();
    let between = Uniform::new_inclusive('0', '9').unwrap();

    let pin = iter::from_fn(move || {
        // generate random pin
        Some(
            between
                .sample_iter(&mut rng)
                .take(pin_len)
                .collect::<String>(),
        )
    })
    .filter(|pin| {
        // must have at least 3 different digits
        pin.chars().unique().count() > 3
        &&
        // must have no sequence of the same digits
        // must have no sequence of asc/desc digits
        !pin.chars()
            .map_into::<u32>()
            .tuple_windows()
            .any(|(a, b)| a == b || a == b + 1 || a + 1 == b)
    })
    .take(1)
    .next()
    .unwrap();

    println!("{}", pin);
}
